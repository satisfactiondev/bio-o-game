﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gerenciador_Respawn : MonoBehaviour {

    private Player_Controller _PlayerScript;
    private GameObject _PlayerInstance;
    private Camera_Comportamento _CameraScript;

    public GameObject PlayerObject;
    public Transform RespawnPlace;

    // Use this for initialization
    void Start()
    {
        _CameraScript = GameObject.Find("Main Camera").GetComponent<Camera_Comportamento>();
        _PlayerInstance = Instantiate(PlayerObject, RespawnPlace);
        _CameraScript.EncontrarPlayer();
        _PlayerScript = GameObject.Find("Bio-o(Clone)").GetComponent<Player_Controller>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_PlayerScript.LifeLost)
        {
			_PlayerScript.LifeLost = false;

            if (_PlayerScript.Life <= 0)
            {
                StartCoroutine(GameOver());
            }

        }
    }

    private IEnumerator GameOver()
    {
        if(_PlayerInstance != null)
        {
            Destroy(_PlayerInstance);
            yield return new WaitForSecondsRealtime(1.5f);
            Recomecar();
        }
    }

    private void Recomecar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
