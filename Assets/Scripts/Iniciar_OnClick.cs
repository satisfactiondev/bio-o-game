﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Iniciar_OnClick : MonoBehaviour {

	public void CarregarPorIndex(int sceneIndex)
    {
		SceneManager.LoadScene(sceneIndex);
	}
}
