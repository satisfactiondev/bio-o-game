﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Comportamento : MonoBehaviour {

    private GameObject _player;

	// Use this for initialization
	void Start ()
    {
	}

    // Update is called once per frame
    void Update()
    {
        // Código tá pesado por ficar criando vetores todo frame.
		if (_player) {
			transform.position = new Vector3 (_player.transform.position.x, _player.transform.position.y + 0.5f, -10);
		} else {
			EncontrarPlayer ();
		}
    }
    public void EncontrarPlayer()
    {
       _player = GameObject.Find("Bio-o(Clone)");
    }
}
