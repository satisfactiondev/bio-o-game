﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Selecionar_OnInput : MonoBehaviour {

	public EventSystem eventSystem;
	public GameObject objetoSelecionado;

	private bool botaoSelecionado;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxisRaw ("Vertical") != 0 && botaoSelecionado == false) {
			eventSystem.SetSelectedGameObject(objetoSelecionado);
			botaoSelecionado = true;
		}
	}

	private void AoDesabilitar(){
		botaoSelecionado = false;
	}
}
